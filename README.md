<h1 align=center>API Permission Example Server</h1>
<h3 align=center>[OpenAPI Schema](https://arivo-public.gitlab.io/device-python/examples/api-permission-example-server/)</h3>

# Overview

If a user wants drive through a gate, our Permission API sends requests to a server. 
The server can then answer with a set of parking permissions, which allows the user to enter.
If a user was granted access by the server, it also receives entry and exit movements of the 
user.

This example server can receive those requests. 

The API sends 3 possible requests:
* Retrieve access permissions for a vehicle
    * Expects a list of [AccessPermissions](#accesspermission) for a user as response
* Check in a vehicle
    * Optional, can be processed if needed
* Check out a vehicle
    * Optional, can be processed if needed


While this example uses mock data for the response, in a real life scenario, the data would be retrieved from a database instead.


# Quickstart

* Install requirements with ```pip install -r requirements.txt```
* Set username and password in the ```.env``` file for API authorization
* Run server with ```bash start.sh```
* Open ```http://localhost:8123/``` in a browser

# Endpoints

The schema is hosted [here](https://arivo-public.gitlab.io/device-python/examples/api-permission-example-server/), 
or can be found at http://localhost:8123/docs when running the server locally.

## Retrieve access permissions for user

```POST /check_access```

### Parameters
The fields of an [AccessRequest](#accessrequest) object

### Returns
Returns a list of [AccessPermissions](#accesspermission)

### Example
Request:
```json
{
  "gate": {
    "gate_id": "gate1",
    "direction": "in"
  },
  "medium_type": "lpr",
  "medium_id": "GJOHN1",
  "resource": "garage1"
}
```

Response:
```json
{
  "access_permissions": [
    {
      "user_id": "john-doe",
      "category": "permanent",
      "start": "2020-01-18T15:00:00",
      "end": "2021-02-24T16:00:00",
      "duration": 6,
      "recurrence": "DTSTART:20200118T120000Z\nRRULE:FREQ=WEEKLY;WKST=MO;BYDAY=MO,TU,WE,TH,FR",
      "zone_id": "zone1",
      "check_door_access": false
    },
    {
      "user_id": "john-doe",
      "category": "booked",
      "start": "2021-01-01T00:00:00",
      "end": "2021-02-02T00:00:00",
      "duration": null,
      "recurrence": null,
      "zone_id": "zone2",
      "check_door_access": false
    }
  ]
}
```

## Check in a vehicle

```POST /check_in```

### Parameters
The fields of a [Passage](#passage) object

### Returns
Returns a status of ```200``` if successful

### Example

Request:
```json
{
  "passage_id": "82ef8760-a999-4472-8d84-f3076f1a210e",
  "user_id": "john-doe",
  "resource": "garage1",
  "gate": {
    "gate_id": "gate1",
    "direction": "in"
  },
  "medium_type": "lpr",
  "medium_id": "GJOHN1",
  "count": 42,
  "zone_id": "zone1",
  "category": "permanent",
  "correction": false,
  "error": null
}
```

## Check out a vehicle

```POST /check_out```

### Parameters
The fields of a [Passage](#passage) object

### Returns
Returns a status of ```200``` if successful

### Example

```json
{
  "passage_id": "448dd3d0-9574-4b2e-abb5-b932c7a05019",
  "user_id": "john-doe",
  "resource": "garage1",
  "gate": {
    "gate_id": "gate1",
    "direction": "out"
  },
  "medium_type": "lpr",
  "medium_id": "GJOHN1",
  "count": 41,
  "zone_id": "zone1",
  "category": "permanent",
  "correction": false,
  "error": null
}
```

# Models

In the following description, if a field is not explicitly **REQUIRED**, it is considered OPTIONAL.

## AccessRequest

An AccessRequest gets generated, if a user wants to enter or exit a gate. 

| **Field name** |          **Type**           | **Description**                                                                       |
| -------------- |:---------------------------:| --------------------------------------------------------------------------------------|
| gate           |         [Gate](#gate)       | The optional gate from where the request to access was sent.                      |
| medium_type    |         ```string```        |**REQUIRED.** Which kind of medium triggered the AccessRequest. <br/> Possible values: ```lpr```, ```nfc```, ```pin```, ```qr```|
| medium_id      |         ```string```        |**REQUIRED.** The unique id of the medium, e.g. license plate if medium_type is ```lpr```. |
| resource       |         ```string```        |**REQUIRED.** The unique identifier of the garage.              |


## AccessPermission

An AccessPermission determines the parking permission for a specific user. 
A user can have multiple AccessPermissions.

| **Field name**    |            **Type**             | **Description**                                                          |
| ----------------- |:-------------------------------:| -------------------------------------------------------------------------|
| user_id           |           ```string```          |**REQUIRED.** A unique string identifying the user.                       |
| category          |            ```string```         |**REQUIRED.** Defines type of access. <br/>Possible values: <br/> <ul><li>```filler```: Can access, if there is a free space. Has to pay the short-term parking rate after ```end``` or outside of the ```recurrence``` rule.</li><li>```permanent```: Can always access, does **not** have to pay the short-term parking rate, after ```end``` or outside of the ```recurrence``` rule.</li><li>```booked```: Can access for a booked time. Has to pay the short-term parking rate after ```end``` or outside of the ```recurrence``` rule.</li></ul>   |
| start             |          ```datetime```         |**REQUIRED.** Start of access permission in UTC.                                 |
| end               |          ```datetime```         | An optional end to the access in UTC. If ```category``` is ```filler``` or ```booked```, the user has to pay after this time.                                           |
| duration          |             ```int```           | The optional duration in seconds. Is only valid, if ```recurrence``` is set.                                                   |
| recurrence        |           ```string```          | Optional [rrule string](https://jakubroztocil.github.io/rrule/), e.g. access permission only valid on every monday. More info at [Validity](#validity).            |
| zone_id           |           ```string```          | A unique string identifying the zone the access permission is valid for. |
| check_door_access |           ```boolean```         | Specifies if a user is allowed to access doors located in the parking lot, if the vehicle is not checked in. |

### Validity

For categories `filler` and `booked`, the user has to pay for the duration of his stay, if he stayed longe than permittet by his permission. Validity is determined by a given timeframe with the parameters `start`, `end`, `duration` and `recurrence`. 

For example, if we want the permission to be valid from 2020-01-18 at 3 p.m., to 2021-02-24 at 4 p.m., 
but only on weekdays for a duration of 6 hours, starting at 12 a.m., we set the parameters like this:

* start: `2020-01-18T15:00:00+00:00`
* end: `2021-02-24T16:00:00+00:00`
* duration: `21600`
    * 6 hours in seconds
* recurrence: ```DTSTART:20200118T120000Z\nRRULE:FREQ=WEEKLY;WKST=MO;BYDAY=MO,TU,WE,TH,FR```
    * Must be a valid [rrule string](https://dateutil.readthedocs.io/en/stable/rrule.html#rrulestr-examples)
    * The DTSTART parameter **must**  be present!. 
    * The DTSTART parameter must be in UTC timezone (ending with `Z`), suffixes such as `+02:00` will be ignored!
    * The `\n` separating the `DTSTART` parameter from `RRULE` is mandatory
    * **For backwards compatibility, currently it is recommended to set `DTSTART` in the rrule to the same value as the `start` field.**

**Example results**
* Monday 2020-01-18: valid from 15:00 - 18:00
* Tuesday 2020-01-19: valid from 12:00 - 18:00
* Wednesday 2020-01-20: valid from 12:00 - 18:00
* ...
* Saturday 2020-01-23: not valid
* Sunday 2020-01-24: not valid
* Monday 2020-01-25: valid from 12:00 - 18:00
* ...
* Wednesday 2021-02-24: valid from 12:00 - 16:00


To build rrules, take a look at the [rrule.js demo](https://jakubroztocil.github.io/rrule/).



## Passage

A Passage object is generated, as soon as a user enters or exits a gate. 

| **Field name** |            **Type**             | **Description**                                                          |
| -------------- |:-------------------------------:| -------------------------------------------------------------------------|
| passage_id     |           ```string```          |**REQUIRED.** A unique string for the current parking transaction. It is the same for the corresponding entry and exit notification if a parking transaction.           |
| user_id        |           ```string```          |**REQUIRED.** A unique string identifying the user.                       |
| resource       |           ```string```          |**REQUIRED.** The unique identifier of the garage.              |
| gate           |           [Gate](#gate)         |**REQUIRED.** The gate to pass through.                                   |
| medium_type    |            ```string```         |**REQUIRED.** Which kind of medium triggered the passage. <br/> Possible values:```lpr```, ```nfc```, ```pin```, ```qr```|
| medium_id      |           ```string```          |**REQUIRED.** The unique id of the medium, e.g. license plate if medium_type is ```lpr```. |
| count          |           ```integer```         |**REQUIRED.** The current occupancy count.             |
| zone_id        |           ```string```          | A unique string identifying the zone the access permission is valid for. |
| category       |            ```string```         |**REQUIRED.** Defines type of access. <br/>Possible values: <br/> <ul><li>```filler```: Can access, if there is a free space. Has to pay the short-term parking rate after ```end``` or outside of the ```recurrence``` rule.</li><li>```permanent```: Can always access, does **not** have to pay the short-term parking rate, after ```end``` or outside of the ```recurrence``` rule.</li><li>```booked```: Can access for a booked time. Has to pay the short-term parking rate after ```end``` or outside of the ```recurrence``` rule.</li></ul>   |
| correction     |           ```boolean```         |**REQUIRED.** Determines if it is an actual passage or a correction of a previous one. See correction section below. |
| error          |            ```string```         |An optional error, e.g. if the passage was a double entry/exit.        |

### Corrections 

A `check_out` / `check_in` request with `correction=True` occurs always for an open parking session, never afterwards. It does not occur for an already closed parking transaction, since there is currently no option to edit closed transactions on the parkinglot itself.

A correction can occur in the following occasions:
  * a user was manually put into the parking lot (`check_in` with `correction=True`)
  * a user was manually removed from the parking lot, e.g. manually closing an open session (`check_out` with `correction=True`)
  * a user drove in twice, with the same `user_id`, and the parking lot is configured to allow this (`check_out` with `correction=True` is sent, with the passage_id of the previous parking transaction)


## Gate

Gateway the user has to pass through for entry or exit.

| **Field name** |          **Type**           | **Description**                                      |
| -------------- |:---------------------------:| -----------------------------------------------------|
| gate_id        |          ```string```       |**REQUIRED.**  The unique identifier of the gate      |
| direction      |          ```string```       |**REQUIRED.** <br/> Possible values:  ```in```, ```out``` or empty string if unknown  |


